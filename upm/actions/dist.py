import subprocess
import datetime

from upm.lib.Script import Script
script = Script()

def dist(repo, args):
    binary = "%s/active/os/nuttx/nuttx.bin" % repo.location
    now = datetime.datetime.utcnow()
    target = "%s/dist/%s - firmware (%s).bin" % (repo.location, now.strftime("%y%m%d"), now.strftime("%H%M%S"))
    subprocess.call(["bash", "-c", "mkdir -p %s/dist" % repo.location])
    subprocess.call(["bash", "-c", "cp \"%s\" \"%s\"" % (binary, target)])

    # Allow running custom script
    # subprocess.call(["rsync", "-a", "--progress", target, "maciek@ks3373847.kimsufi.com:/home/maciek/server/overriden"])

import shutil, subprocess

from upm.lib.Script import Script
script = Script()


def generate(repo, args):
    print()

    if repo.is_os_selected() == False:

        print(script.failure("an os needs to be selected"))
        exit("")

    if args.os_version_file:

        repo.active_os.generate_version_file()
        print(script.success("version written to .version file"))
        exit("")

    if args.app:

        # if args.app == None:
        #     print(script.prefix("provide a name for your new app using --app switch"))
        #     exit("")

        if args.app in repo.apps:
            print(script.prefix("app called [%s] already exists" % args.app))
            print(script.prefix("choose different name or remove existing application"))
            exit("")

        if args.app_blueprint not in repo.blueprints.apps:

            print("blueprints present in selected os:\n")
            script.colprint(repo.blueprints.apps)

            print()
            if args.app_blueprint == None:
                print("select blueprint using --app-blueprint switch")
            else:
                print("blueprint called [%s] doesn't exist" % args.app_blueprint)

            if args.app_blueprint == None:
                print()
                print("example: nuttx generate --app %s --app-blueprint hello" % args.app)

            exit("")

        print(script.prefix("generating an app [%s] from the [%s] blueprint" % (args.app, args.app_blueprint)))

        tmp_apps_location = "%s/apps" % repo.tmp_location

        res_code = subprocess.call(["%s/tools/app_from_blueprint.sh" % script.location, repo.location, repo.tmp_location, args.app, args.app_blueprint])

        tmp_app_location = "%s/%s" % (tmp_apps_location, args.app)
        app_destination = "%s/apps/%s" % (repo.location, args.app)

        if res_code == 0:
            print(script.prefix("moving %s to %s" % (tmp_app_location, app_destination)))
            shutil.move(tmp_app_location, app_destination)
        else:
            script.removal(tmp_apps_location)

        print()

    if args.board:

        # if args.board == None:
        #     print(script.prefix("provide a name for your new board using --board switch"))
        #     exit("")

        if args.board in repo.boards:
            print(script.prefix("board called [%s] already exists" % args.board))
            print(script.prefix("choose different name or remove existing board"))
            exit("")

        if args.board_blueprint not in repo.blueprints.boards:

            print("blueprints present in selected os:\n")
            script.colprint(repo.blueprints.boards)

            print()
            if args.board_blueprint == None:
                print(script.prefix("select blueprint using --board-blueprint switch"))
            else:
                print(script.warning("blueprint called [%s] doesn't exist" % args.board_blueprint))

            if args.board_blueprint == None:
                print()
                print(script.success("nuttx generate --board %s --board-blueprint hello" % args.board))

            exit("")

        print(script.prefix("generating a board [%s] from the [%s] blueprint" % (args.board, args.board_blueprint)))

        tmp_boards_location = "%s/boards" % repo.tmp_location
        res_code = subprocess.call(["%s/tools/board_from_blueprint.sh" % script.location, repo.location, repo.tmp_location, args.board, args.board_blueprint])

        tmp_board_location = "%s/%s" % (tmp_boards_location, args.board)
        board_destination = "%s/boards/%s" % (repo.location, args.board)

        if res_code == 0:
            print(script.prefix("moving [%s] to [%s]" % (tmp_board_location, board_destination)))
            shutil.move(tmp_board_location, board_destination)
        else:
            script.removal(tmp_board_location)

        print();

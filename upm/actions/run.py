import subprocess

from upm.lib.colors import colors

from upm.lib.Script import Script
script = Script()


def run(repo, args):
    if args.skip_build == False and args.list == False:

        # TODO: refuse if no active os is linked, or the link is dead

        compilation_failed = False

        try:
            res_code = subprocess.call(["bash", "-c", "time make --jobs %d" % args.jobs], cwd="%s/active/os/nuttx" % repo.location)
        except (KeyboardInterrupt, ProcessLookupError):
            compilation_failed = True
        finally:
            if compilation_failed:
                print()
                exit(script.prefix("compilation interrupted!"))

        if res_code != 0:
            exit(script.prefix("compilation failed!"))

        # TODO: refuse if binary haven't been created
        #~ res_code = subprocess.call(["bash", "-c", "time autoload nuttx.bin"], cwd="%s/active/os/nuttx" % repo.location)

    if args.for_distribution:
        firmware_filename = "%s.bin" % repo.describe('').replace("/", "-")[0:251]
        subprocess.call(["bash", "-c", "cp active/os/nuttx/nuttx.bin \"dist/%s\"" % firmware_filename], cwd="%s" % repo.location)
        print()
        print(script.prefix("firmware saved in %sdist%s as %s\"%s\"%s" % (colors.OKGREEN, colors.ENDC, colors.OKGREEN, firmware_filename, colors.ENDC)));
        print()
        exit()

    if args.list == True:
        loaders = [name for name in repo.options['loaders']]
        loaders.sort()
    else:
        loaders = repo.options['loader']

    for selected_loader in repo.options['loader']:
        if selected_loader not in repo.options['loaders']:
            print()
            print(script.prefix('%sthere is no loader called [%s]%s' % (colors.FAIL, selected_loader, colors.ENDC)))
            exit("")

    if args.loader != None and args.loader not in loaders:
        print()
        print(script.prefix('%sthere is no loader called [%s]%s' % (colors.FAIL, args.loader, colors.ENDC)))
        exit("")

    if args.list == True:
        print()
        print("known loaders:")
        print()

    for loader_name in loaders:
        loader = repo.options['loaders'][loader_name]

        loader_type = ''

        if 'serial' in loader:
            loader_type = 'serial'
        if 'usb' in loader:
            loader_type = 'usb'

        if args.list == False:

            if loader_type == 'usb':
                for id in loader['usb']:
                    res_code = subprocess.call(["bash", "-c", "lsusb | grep %s > /dev/null" % id])
                    if res_code == 0:
                        print()
                        print(script.prefix("%sstarting %s loader%s" % (colors.OKGREEN, loader_name, colors.ENDC)))
                        print()
                        subprocess.call(["bash", "-c", loader['command'].replace('%binary%', 'active/os/nuttx/nuttx.bin')], cwd=repo.location)
                        exit("")

            if loader_type == 'serial':

                autoconnect = loader['serial']

                if args.loader == loader_name and args.dev != None:
                    autoconnect = [args.dev]

                serial_dev = subprocess.Popen(["%s/tools/get_uucp_dev.sh" % script.location] + autoconnect, stdout=subprocess.PIPE).communicate()[0].decode().strip()

                if len(serial_dev) > 0:
                    print()
                    print(script.prefix("%sstarting %s loader%s" % (colors.OKGREEN, loader_name, colors.ENDC)))
                    print()
                    subprocess.call(["bash", "-c", loader['command'].replace('%binary%', 'active/os/nuttx/nuttx.bin').replace('%dev%', serial_dev)], cwd=repo.location)
                    exit("")

        if args.list == True:
            description = ''
            if 'description' in loader:
                description = loader['description']

            arch = ''
            if 'arch' in loader:
                arch = loader['arch']

            url = ''
            if 'url' in loader:
                url = loader['url']

            # TODO: calculate columns width as a maximum length

            print(colors.OKGREEN, " %-11s" % (loader_name), colors.ENDC, "%-6s %-6s  %-40s %s" % (arch, loader_type, description, url))

    if args.list == False:
        print()
        print(script.prefix('%scannot match any of the configured loaders against your connected hardware%s' % (colors.WARNING, colors.ENDC)))
        print(script.prefix('%sconfigure custom loader, or force existing one using --loader and --dev%s' % (colors.WARNING, colors.ENDC)))
        print()
        print(script.prefix('%syou can list existing loaders with --list%s' % (colors.WARNING, colors.ENDC)))

        # TODO:
        # Your binary is located at ...

    print()

options = {
    'autoconnect': [

        # Based on https://github.com/mik3y/usb-serial-for-android/blob/master/usbSerialForAndroid/src/main/java/com/hoho/android/usbserial/driver/UsbId.java

        "0403:6001",  # FTDI FT232R
        "0403:6015",  # FTDI FT231X
        "03eb:2044",  # Atmel Lufa CDC
        "2341:0001",  # Arduino Uno
        "2341:0010",  # Arduino Mega 2560
        "2341:003b",  # Arduino Serial Adapter
        "2341:003f",  # Arduino Mega ADK
        "2341:0042",  # Arduino Mega 2560 R3
        "2341:0043",  # Arduino Uno R3
        "2341:0044",  # Arduino Mega ADK R3
        "2341:8036",  # Arduino Serial Adapter R3
        "2341:8037",  # Arduino Micro
        "16c0:0483",  # Van Ooijen Tech Teensyduino Serial
        "1eaf:0004",  # Leaflabs Maple
        "10c4:ea60",  # Silabs CP2102
        "10c4:ea70",  # Silabs CP2105
        "10c4:ea71",  # Silabs CP2108
        "10c4:ea80",  # Silabs CP2110
        "067b:2303",  # Prolific PL2303
        "1a86:7523",  # Quinheng HL340

        "0525:a4a7"  # NuttX CDC ACM Serial
    ],

    "loaders": {
        "stlink": {
            "usb": ["0483:3748", "0483:374b"],
            "command": "st-flash write '%binary%' 0x08000000 || st-flash write '%binary%' 0x08000000",

            "arch": "stm32",
            "description": "requires hardware called ST-Link v[1|2]",
            "url": "https://github.com/texane/stlink"
        },
        "stmflasher": {

            # This loader for STM32 can use any serial device, USB-serial adapters as well as internal ports.
            # You will probably want to override this list in your configuration, to match your hardware.

            # Alternatively you can specify correct device on command line.
            # For example: `nuttx run --loader stmflasher --dev /dev/ttyS0`

            "serial": [
                # "067b:2303"  # Prolific PL2303 present in Thaoyu Electronics HY-MiniSTM32V
            ],
            "command": "echo 'Depress reset button, add root button, release reset button, release boot button.' ; stmflasher -p %dev% -w '%binary%' -b 115200 || stmflasher -p %dev% -w '%binary%' -b 115200",

            "arch": "stm32",
            "description": "no hardware required",
            "url": "https://github.com/alatarum/stmflasher",
        }
    },

    "kconfig-frontend": 'menuconfig',  # xconfig --> qt, gconfig --> gtk, menuconfig --> ncurses
    "loader": ["stlink", "stmflasher"],

    "downloaded": [
        "apps/graphics/littlevgl/v5.0.2.tar.gz"
    ]
}

#!/bin/bash

###
# Outline:
# * remove [repo]/tmp/boards/[board] if exists
# * copy selected blueprint to [repo]/tmp/boards/[board] directory
# * rename file %blueprint%_main.c to %board%_main.c
# * replace EXAMPLES_%BLUEPRINT% with CUSTOM_%board% in files
# * replace whole word %blueprint%_main with %board%_main in files
# * replace examples/%blueprint% with external/%board% in Make.defs
# * replace %blueprint% with %board% in Makefile
# * show diff between [repo]/tmp/boards/[board] and the [blueprint]
# * list files that have %blueprint% in their names
# * grep for any %blueprint% to report leftovers
# * initialize git repository
# * move to boards
#
# TODO: aliases were added and move of "%board%.h"
###

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${DIR}/color-helper.bash

# Display a hint if no arguments provided

if [ $# -lt 4 ]; then
  echo "${0##*/} repo_location tmp_location board_name blueprint_name"
  exit 1
fi

repo_location="$1"
tmp_location="$2"
board_name="$3"
blueprint_name="$4"

tmp_board_location="${tmp_location}/boards/${board_name}"
blueprint_location="${repo_location}/active/os/nuttx/configs/${blueprint_name}"

echo

if [ ! -e "${tmp_location}" ]; then
  echo -e "error: ${RED}temporary directory doesn't exist at ${tmp_location}${NC}"
  exit 1
fi

if [ -e "${tmp_board_location}" ]; then
  echo -e "warning: ${RED}a directory have been found at ${tmp_board_location}${NC}"
  exit 1
fi

if [ ! -e "${blueprint_location}" ]; then
  echo -e "error: ${RED}blueprint doesn't exist at ${blueprint_location}${NC}"
  exit 1
fi

mkdir -p "${tmp_location}/boards"
if which rsync &> /dev/null; then
  rsync -a --out-format='%n%L' --exclude "*.o" --exclude ".depend" --exclude ".built" --exclude "Make.dep" "${blueprint_location}/" "${tmp_board_location}/" | tail -n +2 | xargs | sed -e 's/ /, /g'
else
  cp "${blueprint_location}" "${tmp_board_location}"
fi

echo

if [ -e "${tmp_board_location}/Makefile" ]; then
  alt_blueprint_main_name=$(grep MAINSRC "${tmp_board_location}/Makefile" | egrep --color [a-z0-9A-Z_]*_main -o | sed 's/_main//g')
  alt_blueprint_board_name=$(grep BOARDNAME "${tmp_board_location}/Makefile" | egrep --color -o "= [a-z0-9A-Z_]*" | sed 's/= //')
fi

aliases=( "${blueprint_name}" )

if [ ! -z "${alt_blueprint_main_name}" ]; then
  if [ "${alt_blueprint_main_name}" != "${blueprint_name}" ]; then
    aliases+=("${alt_blueprint_main_name}")
    echo -e "info: ${GREEN}detected alternative name on main ${NC}[${alt_blueprint_main_name}]${GREEN}${NC}"
  fi
fi 

if [ ! -z "${alt_blueprint_board_name}" ]; then
  if [ "${alt_blueprint_board_name}" != "${blueprint_name}" ]; then
    if [ "${alt_blueprint_board_name}" != "${alt_blueprint_main_name}" ]; then
      aliases+=("${alt_blueprint_board_name}")
      echo -e "info: ${GREEN}detected alternative name on nsh command [${alt_blueprint_board_name}]${NC}"
    fi
  fi
fi 

for alias in "${aliases[@]}"; do

  if [ -e "${tmp_board_location}" ]; then
    find "${tmp_board_location}" -type f -exec sed -i "s/EXAMPLES_${alias^^}/CUSTOM_${board_name^^}/g" {} +
    find "${tmp_board_location}" -type f -exec sed -i "s/\b${alias}_main\b/${board_name}_main/g" {} +
    find "${tmp_board_location}" -type f -exec sed -i "s/#include \"${alias}.h\"/#include \"${board_name}.h\"/g" {} +
  fi

  if [ -e "${tmp_board_location}/Makefile" ]; then
    sed -i "s/\b${alias}\b/${board_name}/g" "${tmp_board_location}/Makefile"
  fi

  if [ -e "${tmp_board_location}/Make.defs" ]; then
    sed -i "s/\bexamples\/${alias}\b/external\/${board_name}/g" "${tmp_board_location}/Make.defs"
  fi

done

if which meld &> /dev/null; then
  echo -e "info: ${GREEN}process requires manual verification, starting Meld${NC}"
  
  for alias in "${aliases[@]}"; do
    echo -e "info: ${GREEN}will try to rename ${NC}[${alias}_main.{c,cxx}]${GREEN} and ${NC}[${alias}.h]${GREEN} afterwards${NC}"
  done
  
  meld "${blueprint_location}" "${tmp_board_location}" &> /dev/null
else  
  if which colordiff &> /dev/null; then
    colordiff -r "${blueprint_location}" "${tmp_board_location}"
  else
    diff -r "${blueprint_location}" "${tmp_board_location}"
  fi
fi

extensions=( "c" "cxx" "h" )

for alias in "${aliases[@]}"; do
  # TODO: do not move main file if it doesnt end with '_main'
  
  for extension in "${extensions[@]}"; do

    blueprint_main="${alias}_main.${extension}" 
    board_main="${board_name}_main.${extension}" 

    if [ -e "${tmp_board_location}/${blueprint_main}" ]; then
      if [ ! -e "${tmp_board_location}/${board_main}" ]; then
        mv "${tmp_board_location}/${blueprint_main}" "${tmp_board_location}/${board_main}"
        echo -e "info: ${GREEN}renaming ${NC}[${blueprint_main}]${GREEN} to ${NC}[${board_main}]"
      else
        echo -e "warning: ${RED}skipping ${NC}[${blueprint_main}]${RED}, ${NC}[${board_main}]${RED} already exists${NC}"
      fi
    fi
      
  done
  
  if [ -e "${tmp_board_location}/${alias}.h" ]; then
    if [ ! -e "${tmp_board_location}/${board_name}.h" ]; then
      mv "${tmp_board_location}/${alias}.h" "${tmp_board_location}/${board_name}.h"
      echo -e "info: ${GREEN}renaming ${NC}[${alias}.h]${GREEN} to ${NC}[${board_name}.h]"
    else
      echo -e "warning: ${RED}skipping ${NC}[${alias}.h]${RED}, ${NC}[${board_name}.h]${RED} already exists${NC}"
    fi
  fi
done

echo
echo -e "info: ${GREEN}searching for leftovers inside files${NC}"
for alias in "${aliases[@]}"; do
( cd "${tmp_location}/boards" ; find "${board_name}" -type f -exec egrep --color -i "${alias}" {} + )
done

echo
echo -e "info: ${GREEN}searching for leftovers in filenames${NC}"
for alias in "${aliases[@]}"; do
( cd "${tmp_location}/boards" ; find "${board_name}" -iname "*${alias}*" | egrep --color "${alias}")
done

echo

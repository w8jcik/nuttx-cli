#!/bin/bash

# Context-safe effect
# Runs from any place

# TODO: not argument safe

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${DIR}/try-helper.bash
source ${DIR}/color-helper.bash

if [ $# -eq 0 ]; then
  echo "${0##*/} location"
  exit 1
fi

location=$1

if [ -e "${location}" ]; then
  
  location=$(realpath $location)
  name=$(basename $location)

  cd "${location}/nuttx"
  
  standard_name=$(${DIR}/standard_name.sh | tr "-" "\n" | tail -n +2 | paste -sd "-" -)
  major_minor=$(echo "${standard_name}" | tr "-" "\n" | head -n 1 | tail -n 1)
  major=$(echo ${major_minor} | tr "." "\n" | head -n 1)
  minor=$(echo ${major_minor} | tr "." "\n" | tail -n 1)
  date=$(date -u "+%y%m%d%H%m")
  
  echo -e "#!/bin/bash\n" > .version
  echo "CONFIG_VERSION_STRING=\"${standard_name}\"" >> .version
  echo "CONFIG_VERSION_MAJOR=${major}" >> .version
  echo "CONFIG_VERSION_MINOR=${minor}" >> .version
  echo "CONFIG_VERSION_BUILD=\"${date}\"" >> .version

else
  echo "${0##*/}: ${location} doesn't exists"
  exit 1
fi

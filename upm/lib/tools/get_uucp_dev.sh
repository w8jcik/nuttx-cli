#!/bin/bash

allowed=( "$@" )

for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
  syspath="${sysdevpath%/dev}"
  devname="$(udevadm info -q name -p $syspath)"

  [[ "$devname" == "bus/"* ]] && continue
  eval "$(udevadm info -q property --export -p $syspath)"
  [[ -z "$ID_SERIAL" ]] && continue

  uid="${ID_VENDOR_ID}:${ID_MODEL_ID}"
  if [ "${ID_USB_INTERFACE_NUM}" -ne 0 ]; then
    uid="${uid}.${ID_USB_INTERFACE_NUM}"
  fi

  for entry in ${allowed[@]}; do
    if [[ "${uid}" == "${entry}" ]]; then
      echo "/dev/$devname"
      exit 0
    fi
  done
done

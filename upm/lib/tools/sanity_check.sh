#!/bin/bash

# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# 
# source ${DIR}/color-helper.bash
# 
# # TODO: Include checks against different compilers
# 
# if ! which arm-none-eabi-gcc &> /dev/null; then
#   echo ""
#   echo -e "warning: Cannot find arm-none-eabi-gcc. ${RED}You might not be able to compile the firmware.${NC}"
# else
#   version_minor=$(arm-none-eabi-gcc -v 2>&1 > /dev/null | grep "gcc version" | grep -o "[0-9]" | head -n 1)
#   entire_version=$(arm-none-eabi-gcc -v 2>&1 > /dev/null | grep "gcc version" | grep -o "[0-9.]*" | head -n 1)
# 
#   if [ "${version_minor}" -gt 4 ]; then
#     echo ""
#     echo -e "warning: Your cross-compiler version is ${entire_version} (arm-none-eabi-gcc)"
#     echo -e "warning: ${ORANGE}Version higher than 4 is not recommended.${NC}"
#     echo ""
#   fi 
# fi
